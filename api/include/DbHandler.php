<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Zilvinas Kilius
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Check is user exists
     * @param String $username
     */
    public function checkUsername($username) {
        return $this->isUserExists($username);
    }

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $password User login password
     */
    public function createUser($username, $password) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($username)) {
            // Generating password hash
            $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO elt_users(username, password, api_key) values(?, ?, ?)");
            $stmt->bind_param("sss", $username, $password_hash, $api_key);

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $username User login username
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($username, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password AS psw FROM elt_users WHERE username = ?");

        $stmt->bind_param("s", $username);

        $stmt->execute();

        $stmt->bind_result($psw);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the username
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($psw, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($username) {
        $stmt = $this->conn->prepare("SELECT id from elt_users WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by username
     * @param String $username
     */
    public function getUserByUsername($username) {
        $stmt = $this->conn->prepare("SELECT username, api_key FROM elt_users WHERE username = ?");
        $stmt->bind_param("s", $username);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($username, $api_key);
            $stmt->fetch();
            $user = array();
            $user["username"] = $username;
            $user["api_key"] = $api_key;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id FROM elt_users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id from elt_users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Fetching all category types
     */
    public function getAllTypes() {
        $stmt = $this->conn->prepare("SELECT * FROM elt_category_types");
        $stmt->execute();
        $types = $stmt->get_result();
        $stmt->close();
        return $types;
    }

    /**
     * Fetching all categories of type
     */
    public function getCategoriesByType($typeId) {
        $stmt = $this->conn->prepare("SELECT * FROM elt_categories WHERE typeid = ?");
        $stmt->bind_param("i", $typeId);
        $stmt->execute();
        $categories = $stmt->get_result();
        $stmt->close();
        return $categories;
    }

    /**
     * Fetching all category levels
     */
    public function getLevelsByCategory($catId) {
        $stmt = $this->conn->prepare("SELECT * FROM elt_levels WHERE catId = ?");
        $stmt->bind_param("i", $catId);
        $stmt->execute();
        $categories = $stmt->get_result();
        $stmt->close();
        return $categories;
    }

    public function getUserStars($levelId, $userId) {
        $stmt = $this->conn->prepare("SELECT stars FROM elt_userstars2level WHERE levelId = ? AND userId = ?");
        $stmt->bind_param("ii", $levelId, $userId);
        $stmt->execute();
        $stars = $stmt->get_result();
        $stmt->close();
        return $stars->fetch_assoc();
    }

    /**
     * Fetching all level info
     */
    public function getLevel($id) {
        $stmt = $this->conn->prepare("SELECT * FROM elt_levels WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $level = $stmt->get_result();
        $stmt->close();
        return $level;
    }

    /**
     * Fetching correct answer
     */
    public function getCorrect($id) {
        $stmt = $this->conn->prepare("SELECT correct FROM elt_tasks WHERE id = ?");
        $stmt->bind_param("s", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $result = $result->fetch_assoc();
        return $result['correct'];
    }

    /**
     * Fetching current user stars
     */
    public function getCurrentStars($levelId, $userId) {
        $stmt = $this->conn->prepare("SELECT stars FROM elt_userstars2level WHERE levelId = ? and userId = ?");
        $stmt->bind_param("ii", $levelId, $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $result = $result->fetch_assoc();
        return $result['stars'];
    }

    /**
     * Updating user exp
     */
    public function updateExp($userId, $reward) {
        $stmt = $this->conn->prepare("SELECT exp FROM elt_users WHERE id = ?");
        $stmt->bind_param("s", $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $exp = $result->fetch_assoc();
        $exp = $exp['exp'] + $reward;
        $stmt = $this->conn->prepare("UPDATE elt_users SET exp = ? WHERE id = ?");
        $stmt->bind_param("ii", $exp, $userId);
        $stmt->execute();
        $stmt->close();
    }

    /**
     * Updating user stars
     */
    public function updateStars2Level($levelId, $userId, $stars) {
            $stmt = $this->conn->prepare("SELECT id FROM elt_userstars2level WHERE levelId = ? AND userId = ?");
            $stmt->bind_param("ii", $levelId, $userId);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            $id = $result->fetch_assoc();

            if(is_null($id['id'])) {
                // insert query
                $stmt = $this->conn->prepare("INSERT INTO elt_userstars2level(levelId, userId, stars) values(?, ?, ?)");
                $stmt->bind_param("iii", $levelId, $userId, $stars);

                $stmt->execute();

                $stmt->close();
            } else {

                $stmt = $this->conn->prepare("UPDATE elt_userstars2level SET stars = ? WHERE id = ?");
                $stmt->bind_param("ii", $stars, $id['id']);

                $stmt->execute();

                $stmt->close();
            }


        return;
    }

    /**
     * Updating user level
     */
    public function checkIfLevelUp($userId) {
        $stmt = $this->conn->prepare("SELECT exp, level FROM elt_users WHERE id = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $tmp = $result->fetch_assoc();
        $level = $tmp['level'] + 1;

        $stmt = $this->conn->prepare("SELECT exp FROM elt_exp_milestones WHERE level = ?");
        $stmt->bind_param("i", $level);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        $tmp2 = $result->fetch_assoc();

        if($tmp['exp'] >= $tmp2['exp']) {
            $stmt = $this->conn->prepare("UPDATE elt_users SET level = ? WHERE id = ?");
            $stmt->bind_param("ii", $level, $userId);

            $stmt->execute();

            $stmt->close();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Fetching all level info
     */
    public function getTasksByLevel($id, $cnt) {
        $stmt = $this->conn->prepare("SELECT * FROM elt_tasks WHERE level_id = ? ORDER BY RAND() LIMIT ?");
        $stmt->bind_param("ii", $id, $cnt);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

    /**
     * Fetching all user info
     */
    public function getUserInfo($user_id) {
        $stmt = $this->conn->prepare("SELECT * FROM elt_users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $categories = $stmt->get_result();
        $stmt->close();
        return $categories;
    }

    /**
     * Get user exp
     */
    public function getUserExp($user_id) {
        $stmt = $this->conn->prepare("SELECT exp FROM elt_users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $stmt->bind_result($exp);
            $stmt->fetch();
            $stmt->close();
            return $exp;
        } else {
            return NULL;
        }
    }

    /**
     * Get next level exp
     */
    public function getNextLevel($next) {
        $stmt = $this->conn->prepare("SELECT exp FROM elt_exp_milestones WHERE level = ?");
        $stmt->bind_param("i", $next);
        if ($stmt->execute()) {
            $stmt->bind_result($exp);
            $stmt->fetch();
            $stmt->close();
            return $exp;
        } else {
            return NULL;
        }
    }



    /* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $task, $status, $created_at);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["task"] = $task;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }



    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }

}

?>

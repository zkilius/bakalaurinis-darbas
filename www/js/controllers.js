angular.module('starter.controllers', ['ngResource','ionic.ion.imageCacheFactory', 'ionic.utils'])

    .controller('LoginCtrl', function ($scope, LoginService, $ionicPopup, $state, $http, Login, $timeout, $localstorage) {
        $scope.loginData = {};
        $scope.errorMessage = false;

        $scope.loginShow = true;

        $scope.login = function () {
            if($scope.loginData.username && $scope.loginData.password) {
                var login = new Login();
                login.username = $scope.loginData.username;
                login.password = $scope.loginData.password;

                login.$save()
                    .then(function (res) {
                        if (res.error) {
                            $scope.errorMessage = res.message;
                            $scope.userError = true;
                            $timeout(function () {
                                $scope.userError = false;
                            }, 800);
                        } else {
                            $scope.loginShow = false;
                            $timeout(function() {
                                $state.go('register');
                                $localstorage.set('apiKey', res.apiKey);
                                $state.go('home');
                                $scope.loginShow = true;
                            }, 400);
                        }
                    });
            } else {
                $scope.errorMessage = 'Reikia užpildyti visus laukus';
                $scope.emptyFieldsError = true;
                $timeout(function () {
                    $scope.emptyFieldsError = false;
                }, 800);
            }
        }

        $scope.registration = function() {
            $scope.loginShow = false;
            $timeout(function() {
                $state.go('register');
                $scope.loginShow = true;
            }, 400);
        }
    })

    .controller('RegisterCtrl', function ($scope, $http, $state, Register, $timeout, CheckUsername, $localstorage) {

        $scope.registerData = {};

        $scope.index = true;
        $scope.username = 'hidden';
        $scope.avatar = 'hidden';
        $scope.emptyFieldsError = false;
        $scope.errorMessage = false;

        $scope.nextStep = function(current) {
            switch (current) {
                case 'index':
                    $scope.index = false;
                    $timeout(function() {
                        $scope.username = true;
                        $scope.index = 'hidden';
                        }, 400);
                    break;
                case 'username':
                    if($scope.registerData.username && $scope.registerData.password && $scope.registerData.passwordConfirm ) {
                        //console.log(validateForm($scope.registerData));
                        var checkUsername = new CheckUsername({username: $scope.registerData.username});
                        checkUsername.$get()
                            .then(function (res) {
                                if (res.result == true) {
                                    $scope.errorMessage = 'Toks vartotojas jau yra';
                                    $scope.usernameError = true;
                                    $timeout(function () {
                                        $scope.usernameError = false;
                                    }, 800);
                                } else {
                                    if($scope.registerData.password === $scope.registerData.passwordConfirm) {
                                        $scope.username = false;
                                        $timeout(function () {
                                            $scope.avatar = true;
                                            $scope.username = 'hidden';
                                            $scope.errorMessage = false;
                                        }, 400);
                                    } else {
                                        $scope.errorMessage = 'Slaptažodžiai nesutampa';
                                        $scope.passwordError = true;
                                        $timeout(function () {
                                            $scope.passwordError = false;
                                        }, 800);
                                    }
                                }
                            })
                    }else{
                        $scope.errorMessage = 'Reikia užpildyti visus laukus';
                        $scope.emptyFieldsError = true;
                        $timeout(function () {
                            $scope.emptyFieldsError = false;
                        }, 800);
                    }
                    break;
            }

        };

        $scope.register = function () {
            var registration = new Register($scope.registerData);

            registration.$save()
                .then(function(res){
                    if(res.error) {
                        alert(res.message);
                    } else {
                        $scope.avatar = false;
                        $localstorage.set('firstTime', false);
                        $state.go('login');
                    }
                });
        }
    })
    .controller('HomeCtrl', function ($scope, Types, Categories, $ionicScrollDelegate, $timeout, User, $ionicLoading, $ionicPopup, $ImageCacheFactory, Levels, $state) {

        $scope.activeExtend = false;
        $scope.activeLevel = 0;
        $scope.activeType = 0;
        $scope.levelType = 0;

        $timeout(function() {
            //$ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'})
        }, 0);

        var user = new User();

        user.$get()
            .then(function(res){
                if(res.error) {
                    alert(res.message);
                } else {
                    $scope.user = angular.fromJson(res.user[0]);
                }
                var types = new Types();

                types.$get()
                    .then(function(res){
                        if(res.error) {
                            alert(res.message);
                        } else {
                            $scope.types = angular.fromJson(res.types);
                            $scope.getCategoriesByType($scope.types[0].id, 0);
                            $scope.activeType = $scope.types[0].id;
                        }

                        var startColor = '#db3e3e';
                        var endColor = '#379ad9';

                        var element = document.getElementById('progress');
                        var circle = new ProgressBar.Circle(element, {
                            color: startColor,
                            trailColor: '#f3c869',
                            trailWidth: 8,
                            duration: 3000,
                            easing: 'bounce',
                            strokeWidth: 3,

                            // Set default step function for all animate calls
                            step: function(state, circle) {
                                circle.path.setAttribute('stroke', state.color);
                            }
                        });


                        circle.animate(0.6, {
                            from: {color: startColor},
                            to: {color: endColor}
                        });

                    });
            });

        $scope.getCategoriesByType = function ($typeId, $type) {
            $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});
            var categories = new Categories({typeId: $typeId});

            categories.$get()
                .then(function (res) {
                    if (res.error) {
                        alert(res.message);
                    } else {
                        var categories = angular.fromJson(res.categories);
                        var images = [];
                        for(var i=0; i<categories.length;i++){
                            images.push(categories[i].image);
                        }

                        $ImageCacheFactory.Cache(images).then(function(){
                            $ionicLoading.hide();
                            $scope.categories = categories;
                            $scope.activeType = $typeId;
                            $scope.levelType = $type;
                            $timeout(function() {
                                $ionicScrollDelegate.$getByHandle('categoriesScroll').scrollTop(true);
                            }, 0);
                        });
                    }
                });
        }

        $scope.loadLevels = function(id) {
            $ionicLoading.show({template: '<ion-spinner icon="android"></ion-spinner>'});

            var levels = new Levels({catId: id});

            levels.$get()
                .then(function (res) {
                    if (res.error) {
                        alert(res.message);
                    } else {
                        $scope.levels = angular.fromJson(res.levels);
                        $ionicLoading.hide();

                        $( "#cat"+id ).slideToggle( "normal", function() {
                            // Animation complete.
                        });
                        if($scope.activeExtend != false && $scope.activeExtend != id) {
                            $( "#cat"+ $scope.activeExtend).slideToggle( "normal", function() {
                                // Animation complete.
                            });
                        }
                        if($scope.activeExtend != id) {
                            $scope.activeExtend = id;
                        } else {
                            $scope.activeExtend = false;
                        }
                    }
                });
        }

        $ionicLoading.hide();

        $scope.goToLevel = function($levelType, $active) {
            $state.go('level', {id: $scope.levels[$active].id, levelType: $scope.types[$scope.levelType].abbr});
        }
    })
    .controller('CategoryCtrl', function($scope, $state, $stateParams, Levels) {

        $scope.params = $stateParams;

        var levels = new Levels({catId: $scope.params.id});

        levels.$get()
            .then(function (res) {
                if (res.error) {
                    alert(res.message);
                } else {
                    $scope.levels = angular.fromJson(res.levels);
                }
            });

    })

    .controller('LevelCtrl', function($scope, $state, $stateParams, Level, validateAnswer, updateUserExp, $timeout, $ionicPopup) {

        $scope.params = $stateParams;

        $scope.index = 0;

        $scope.hearts = 3;

        var level = new Level({levelId: $scope.params.id});

        level.$get()
            .then(function (res) {
                if (res.error) {
                    alert(res.message);
                } else {
                    $scope.level = res.level[0];
                    $scope.loadTask($scope.index);
                }
            });

        $scope.loadTask = function($index){
            $scope.image = $scope.level.tasks[$index].image;
            $scope.title = 'Klausimas ' + ($index+1) + ' iš ' + $scope.level.taskcount;
            $scope.add = $scope.level.tasks[$index].additional;
            $scope.id = $scope.level.tasks[$index].id;
            if($scope.params.levelType == 'atpazink') {
                var answers = $scope.level.tasks[$index].answers.split(";");
                var random = [];
                random[0] = $scope.level.tasks[$index].correct.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                    return letter.toUpperCase();
                });
                for (i = 1; i <= 3;) {
                    var rand = answers[Math.floor(Math.random() * answers.length)];
                    rand = rand.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                        return letter.toUpperCase();
                    });
                    if ($.inArray(rand, random) == -1) {
                        random[i] = rand;
                        i++;
                    }
                }
                shuffle(random);

                $scope.answers = random;
            } else {
                console.log('ok');
            }
        }

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex ;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        $scope.validateAnswer = function(id, answer) {
            var validation = new validateAnswer({taskId: id, answer: answer});

            validation.$get()
                .then(function (res) {
                    if (res.error) {
                        alert(res.message);
                    } else {
                       if(!res.result) {
                           if($scope.hearts - 1 == 0) {
                               $scope.popup = $ionicPopup.show({
                                   templateUrl: 'templates/popups/popup-over.html',
                                   scope: $scope
                               });
                               $state.go('home');
                               resetLevel();
                           } else {
                               $scope.popup = $ionicPopup.show({
                                   templateUrl: 'templates/popups/popup-incorrect.html',
                                   scope: $scope
                               });
                               $scope.hearts -= 1;
                           }
                       } else {
                           if($scope.level.tasks.length <= $scope.index + 1) {
                               var reward = calculateAward($scope.hearts, $scope.level.reward);

                               var updateExp = new updateUserExp({levelId: $scope.level.id, reward:  $scope.level.reward, stars: $scope.hearts});

                               updateExp.$get()
                                   .then(function (res) {
                                       if(res.result) {
                                           $timeout(function() {
                                               $state.transitionTo('home', {}, { reload: true, location: 'replace' });
                                               resetLevel();
                                           }, 400);
                                       }
                                   });
                           } else {
                               $scope.index += 1;
                               $scope.loadTask($scope.index);
                           }
                       }
                    }
                });
        }

        function resetLevel() {
            $scope.index = 0;
            $scope.hearts = 3;
            $scope.loadTask($scope.index);
        }

        function calculateAward(hearts, reward) {
            return Math.round((reward/3) * hearts);
        }

        $scope.closePopup = function() {
            $scope.popup.close();
        }

    })
;

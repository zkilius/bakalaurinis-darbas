angular.module('starter.services', ['ionic.utils'])

    .service('LoginService', function($q, $http) {
      return {
        loginUser: function(status) {
          var deferred = $q.defer();
          var promise = deferred.promise;

          if (status == "success") {
            deferred.resolve('Welcome!');
          } else {
            deferred.reject('Wrong credentials.');
          }
          promise.success = function(fn) {
            promise.then(fn);
            return promise;
          }
          promise.error = function(fn) {
            promise.then(null, fn);
            return promise;
          }
          return promise;
        }
      }
    })

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})
    .factory('ApiService', function() {
      return {
        url : 'http://zilkil.com/eltukas/api/v1'
      };
    })
    .factory('Login', function($resource, ApiService) {
      return $resource(ApiService.url + '/login')
    })
    .factory('CheckUsername', function($resource, ApiService) {
      return $resource(ApiService.url + '/checkusername/:username',{username: '@username'})
    })
.factory('Register', function($resource, ApiService) {
  return $resource(ApiService.url + '/register')
})
    .factory('Types', function($resource, ApiService) {
      return $resource(ApiService.url + '/types')
    })
    .factory('Categories', function($resource, ApiService) {
      return $resource(ApiService.url + '/categories/:typeId',{typeId: '@typeId'})
    })

    .factory('Levels', function($resource, ApiService, $http, $localstorage) {
      $http.defaults.headers.common.Authorization = $localstorage.get('apiKey');
      return $resource(ApiService.url + '/levels/:catId',{catId: '@catId'})
    })

    .factory('Level', function($resource, ApiService, $http, $localstorage) {
        $http.defaults.headers.common.Authorization = $localstorage.get('apiKey');
      return $resource(ApiService.url + '/level/:levelId',{levelId: '@levelId'})
    })

    .factory('User', function($resource, ApiService, $http, $localstorage) {
        $http.defaults.headers.common.Authorization = $localstorage.get('apiKey');
      return $resource(ApiService.url + '/user/')
    })

    .factory('validateAnswer', function($resource, ApiService, $http, $localstorage) {
        $http.defaults.headers.common.Authorization = $localstorage.get('apiKey');
      return $resource(ApiService.url + '/validateAnswer/:taskId/:answer', {taskId: '@taskId', answer: '@answer'})
    })

    .factory('updateUserExp', function($resource, ApiService, $http, $localstorage) {
        $http.defaults.headers.common.Authorization = $localstorage.get('apiKey');
        return $resource(ApiService.url + '/updateUserExp/:levelId/:reward/:stars', {levelId: '@levelId', reward: '@reward', stars:'@stars'})
    })

;

<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
if ( "OPTIONS" === $_SERVER['REQUEST_METHOD'] ) {
    die();
}

// Handling POST request json params
if ($_SERVER['REQUEST_METHOD'] == 'POST' && strpos($_SERVER["CONTENT_TYPE"], 'application/json') !== false) {
    $_REQUEST = json_decode(file_get_contents('php://input'), true);
    $_POST = json_decode(file_get_contents('php://input'), true);
}

require_once '../include/DbHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */

/**
 * url - /checkusername/:username
 * method - GET
 * params - username
 */
$app->get('/checkusername/:username', function($username) use ($app) {

    $db = new DbHandler();
    $response['result'] = $db->checkUsername($username);

    // echo json response
    echoRespnse(200, $response);
});

/**
 * User Registration
 * url - /register
 * method - POST
 * params - username, password
 */
$app->post('/register', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('username', 'password'));

            $response = array();

            // reading post params
            $username = $app->request->post('username');
            $password = $app->request->post('password');

            $db = new DbHandler();
            $res = $db->createUser($username, $password);

            if ($res == USER_CREATED_SUCCESSFULLY) {
                $response["error"] = false;
                $response["message"] = "Registracija sėkminga";
            } else if ($res == USER_CREATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "Ups! Registracija nepavyko";
            } else if ($res == USER_ALREADY_EXISTED) {
                $response["error"] = true;
                $response["message"] = "Deja, toks vartotojas jau yra";
            }
            // echo json response
            echoRespnse(201, $response);
        });

/**
 * User Login
 * url - /login
 * method - POST
 * params - username, password
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('username', 'password'));

            // reading post params
            $username = $app->request()->post('username');
            $password = $app->request()->post('password');
            $response = array();

            $db = new DbHandler();
            // check for correct username and password
            if ($db->checkLogin($username, $password)) {
                // get the user by username
                $user = $db->getUserByUsername($username);

                if ($user != NULL) {
                    $response["error"] = false;
                    $response['username'] = $user['username'];
                    $response['apiKey'] = $user['api_key'];
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "Klaida. Bandykite dar kartą";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Neteisingi prisijungimo duomenys';
            }

            echoRespnse(200, $response);
        });

/**
 * Listing all category types
 * method GET
 * url /types
 */
$app->get('/types', function() {
    $response = array();
    $db = new DbHandler();

    // fetching all user tasks
    $result = $db->getAllTypes();

    $response["error"] = false;
    $response["types"] = array();

    // looping through result and preparing tasks array
    while ($type = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $type["id"];
        $tmp["title"] = $type["title"];
        $tmp["description"] = $type["description"];
        $tmp["abbr"] = $type["abbr"];
        array_push($response["types"], $tmp);
    }

    echoRespnse(200, $response);
});

/**
 * Listing all category types
 * method GET
 * url /categories/:typeId
 * params -typeId
 */
$app->get('/categories/:typeId', function($typeId) use($app) {

    $response = array();
    $db = new DbHandler();

    // fetching all categories by type
    $result = $db->getCategoriesByType($typeId);

    $response["error"] = false;
    $response["categories"] = array();

    // looping through result and preparing categories array
    while ($category = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $category["id"];
        $tmp["title"] = $category["title"];
        $tmp["description"] = $category["description"];
        $tmp["image"] = $category["image"];
        $tmp["typeid"] = $category["typeid"];
        array_push($response["categories"], $tmp);
    }

    echoRespnse(200, $response);
});

/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/**
 * Listing all category types
 * method GET
 * url /levels/:catId
 * params -catId,
 */
$app->get('/levels/:catId', 'authenticate', function($catId) use($app) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    // fetching all categories by type
    $result = $db->getLevelsByCategory($catId, $user_id);

    $response["error"] = false;
    $response["levels"] = array();

    // looping through result and preparing categories array
    while ($level = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $level["id"];
        $tmp["title"] = $level["title"];
        $tmp["image"] = $level["image"];
        $tmp["locked"] = isLocked($level["minExp"], $user_id);
        $tmp["userstars"] = getUserStars($tmp["id"], $user_id);
        array_push($response["levels"], $tmp);
    }

    echoRespnse(200, $response);
});

/**
 * Returns level data
 * method GET
 * url /level/:id
 * params -id,
 */
$app->get('/level/:id', 'authenticate', function($level_id) use($app) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    // fetching all categories by type
    $result = $db->getLevel($level_id);

    $response["error"] = false;
    $response["level"] = array();

    // looping through result and preparing categories array
    while ($level = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $level["id"];
        $tmp["title"] = $level["title"];
        $tmp["reward"] = $level["reward"];
        $tmp["taskcount"] = $level["taskcount"];
        $tmp["tasks"] = getTasks($level_id, $tmp["taskcount"]);
        array_push($response["level"], $tmp);
    }

    echoRespnse(200, $response);
});

/**
 * Listing all category types
 * method GET
 * url /validateAnswer/:taskId/:answer
 * params -taskId, answer
 */
$app->get('/validateAnswer/:taskId/:answer', 'authenticate', function($taskId, $answer) use($app) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    // fetching all categories by type
    $correct = $db->getCorrect($taskId);

    $response["error"] = false;

    if($correct == strtolower($answer)) {
        $response["result"] = true;
    } else {
        $response["result"] = false;
    }
    echoRespnse(200, $response);
});

/**
 * Listing all category types
 * method GET
 * url /updateUserExp/:levelId/:reward/:stars
 * params -levelId, reward, stars
 */
$app->get('/updateUserExp/:levelId/:reward/:stars', 'authenticate', function($levelId, $reward, $stars) use($app) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    $currentStars = checkCurrentStars($levelId, $user_id, $stars);

    $response["error"] = false;
    $response["result"] = true;

    if(is_int($currentStars)) {
        $reward = round(($reward / 3) * $stars) - round(($reward / 3) * $currentStars);
        $db->updateExp($user_id, $reward);
        $db->updateStars2Level($levelId, $user_id, $stars);

        if ($db->checkIfLevelUp($user_id)) {
            $response["error"] = false;
            $response["result"] = 'levelup';
        }
    }
    echoRespnse(200, $response);
});

/**
 * Return user info
 * method GET
 * url /user/
 */
$app->get('/user/', 'authenticate', function() use($app) {
    global $user_id;
    $response = array();
    $db = new DbHandler();

    // fetching all categories by type
    $result = $db->getUserInfo($user_id);

    $response["error"] = false;
    $response["user"] = array();

    // looping through result and preparing categories array
    while ($user = $result->fetch_assoc()) {
        $tmp = array();
        $tmp["exp"] = $user["exp"];
        $tmp["level"] = $user["level"];
        $tmp["avatar"] = $user["avatar"];
        $tmp["nextLevel"] = nextLevel($user["level"]);
        array_push($response["user"], $tmp);
    }

    echoRespnse(200, $response);
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }

    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

function isLocked($minExp, $user_id) {
    $db = new DbHandler();

    $userExp = $db->getUserExp($user_id);
    if ($userExp < $minExp) {
        return true;
    } else {
        return false;
    }
}

function getUserStars($levelId, $userId) {
    $db = new DbHandler();

    $stars = $db->getUserStars($levelId, $userId);

    return (!isset($stars["stars"]) || !is_int($stars["stars"]) ? 0 : $stars["stars"]);
}

function nextLevel($current) {
    $db = new DbHandler();

    $next = (int) $current + 1;

    return $db->getNextLevel($next);
}

function getTasks($level_id, $cnt) {
    $db = new DbHandler();

    $tasks = $db->getTasksByLevel($level_id, $cnt);

    $return = array();

    // looping through result and preparing categories array
    while ($task = $tasks->fetch_assoc()) {
        $tmp = array();
        $tmp["id"] = $task["id"];
        $tmp["image"] = $task["image"];
        $tmp["correct"] = $task["correct"];
        $tmp["answers"] = $task["answers"];
        $tmp["additional"] = $task["additional"];
        array_push($return, $tmp);
    }

    return $return;
}

function checkCurrentStars($levelId, $userId, $stars) {
    $db = new DbHandler();

    $currentStars = $db->getCurrentStars($levelId, $userId);

    if(!isset($currentStars) || $currentStars < $stars) {
        return (!isset($currentStars) ? 0 : $currentStars);
    } else {
        return false;
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
?>